# swaggercustomservices

This repository is for creating models from swagger json


This page describes about generating customized lombok annotated models from swagger json.

This consumes swagger json from microservice endpoints.

Results: Routes and models will be generated if the swagger is properly constructed.



Steps to generate:
Clone the https://bitbucket.org/madhan_dev/swaggercustomservices/src/master/ repository to local PC.
Pre requisites: Java, Maven
Browse through the directory
run mvn clean install.
jars will be installed on local pc.
Integration:
1. https://bitbucket.org/skava-admin/ecommorchestration/src/buildline/pom.xml

maven build plugin has been added

2. mvn swagger2cxf:generate to generate models and routes

3. Other configuration in swagger_generation.properties