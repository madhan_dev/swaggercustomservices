/*******************************************************************************
 * Copyright ©2002-2018 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/

package com.skava.plugins;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.maven.model.Resource;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import io.swagger.codegen.ClientOptInput;
import io.swagger.codegen.ClientOpts;
import io.swagger.codegen.CodegenConfig;
import io.swagger.codegen.CodegenConstants;
import io.swagger.codegen.DefaultGenerator;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.Swagger;
import io.swagger.parser.SwaggerParser;

/**
 * Goal which touches a timestamp file.
 */
@Mojo(name = "generate", defaultPhase = LifecyclePhase.NONE)
public class APIGenMojo extends AbstractMojo {

  /**
   * Location of the output directory.
   */
  @Parameter(name = "output",
    property = "swagger.codegen.maven.plugin.output",
    defaultValue = "${project.basedir}/src/main/java")
  private File output;

  /**
   * Location of the swagger spec, as URL or file.
   */
  @Parameter(required = false)
  private String inputSpec;

  @Parameter(name = "inputFile", property = "swagger.codegen.maven.plugin.input")
  private String inputFile;

  @Parameter(name = "modelPrefix", property = "swagger.codegen.maven.plugin.modelPrefix", defaultValue = "com.skava")
  private String modelPrefix;

  @Parameter(name = "modelSuffix", property = "swagger.codegen.maven.plugin.modelSuffix", defaultValue = "models.dto")
  private String modelSuffix;

  @Parameter(name = "xmlPackage", property = "swagger.codegen.maven.plugin.xmlPackage",
    defaultValue = "/src/main/resources/gen")
  private String xmlPackage;

  @Parameter(defaultValue = "${project}", readonly = true)
  private MavenProject project;

  @Parameter(defaultValue = "true", property = "swagger.codegen.maven.plugin.generateModels", name = "generateModels")
  private boolean generateModels;

  @Parameter(defaultValue = "true", property = "swagger.codegen.maven.plugin.generateRoutes", name = "generateRoutes")
  private boolean generateRoutes;

  public void execute()
    throws MojoExecutionException {
    @SuppressWarnings("unchecked")
    List<Resource> resources = project.getResources();
    final String fileToFind = resources.get(0).getDirectory() + File.separator + inputFile;
    final String exportedXMLPackage = resources.get(0).getDirectory() + File.separator + xmlPackage + File.separator;
    System.setProperty(CodegenConstants.API_DOCS, String.valueOf(Boolean.FALSE.booleanValue()));
    System.setProperty(CodegenConstants.API_TESTS, String.valueOf(Boolean.FALSE.booleanValue()));
    System.setProperty(CodegenConstants.MODEL_DOCS, String.valueOf(Boolean.FALSE.booleanValue()));
    System.setProperty(CodegenConstants.MODEL_TESTS, String.valueOf(Boolean.FALSE.booleanValue()));
    System.setProperty(CodegenConstants.SOURCE_FOLDER, File.separator);

    Map<String, ServiceRegistryProperties> prop = null;
    if (inputFile != null) {
      getLog().info("FileName=" + inputFile);
      try {
        prop = RouteGenerator.loadProperties(fileToFind);
      } catch (IOException e) {
        getLog().error("Error on loading properties " + fileToFind);
      }
      String modelsParent = modelPrefix + ".";
      if (generateModels) {
        Iterator<Map.Entry<String, ServiceRegistryProperties>> iterator = prop.entrySet().iterator();
        while (iterator.hasNext()) {
          Map.Entry<String, ServiceRegistryProperties> entry = iterator.next();
          getLog().info(fileToFind + ":: Key=" + entry.getKey() + "::value=" + entry.getValue().toString());
          getLog().info("skipGen = " + entry.getValue().isSkipGenerationModels());
          if (!entry.getValue().isSkipGenerationModels()) {
            try {
              Swagger swagger = new SwaggerParser()
                .read((entry.getValue().getIp() + "/" + entry.getValue().getContextPath() + "/v2/api-docs"));
              keepOnlySingleTagPerOperation(swagger);
              CodegenConfig config = new CxfCodeGen();
              String modelsLoader = modelsParent + entry.getKey() + "." + modelSuffix;
              config.additionalProperties().put("modelPackage", modelsLoader);
              if (entry.getValue().getModelsToGenerate() != null) {
                System.setProperty(CodegenConstants.MODELS, entry.getValue().getModelsToGenerate());
              } else {
                System.clearProperty(CodegenConstants.MODELS);
              }
              config.setOutputDir(output.getAbsolutePath());

              ClientOptInput input = new ClientOptInput().opts(new ClientOpts()).swagger(swagger);
              input.setConfig(config);
              new DefaultGenerator().opts(input).generate();
            } catch (Exception e) {
              getLog().error("Error on convert model " + e.getCause());
            }
          }
        }
      }
      if (generateRoutes) {
        RouteGenerator.generateRoutes(fileToFind, modelsParent, modelSuffix, exportedXMLPackage);
      }
    }
  }

  /** This is a workaround fix to avoid generating duplicated methods when there are 
   * multiple tags per operation
   * 
   * @param swagger
   */
  private void keepOnlySingleTagPerOperation(Swagger swagger) {
    for (Path path : swagger.getPaths().values()) {
      for (Operation op : path.getOperations()) {
        List<String> tags = op.getTags();
        if (tags != null) {
          Iterator<String> iterator = tags.iterator();
          boolean first = true;
          while (iterator.hasNext()) {
            if (first) {
              first = false;
            } else {
              iterator.remove();
            }
            iterator.next();
          }
        }
      }
    }
  }

}
