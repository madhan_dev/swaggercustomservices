/*******************************************************************************
 * Copyright ©2002-2018 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.plugins;

import lombok.Getter;
import lombok.Setter;

/**
 * <p>ServiceRegistryProperties class.</p>
 *
 * @author Muthu prakash M
 * @version 8.0
 * This class is used for kubernetes service discovery
 * We can specify serviceName is we want to call other services using servicename.
 * We can also specify ip and port.
 */
public class ServiceRegistryProperties {

  @Getter
  @Setter
  private String serviceName;
  @Getter
  @Setter
  private String version;
  @Getter
  @Setter
  private String ip;
  @Getter
  @Setter
  private String port;

  @Getter
  @Setter
  private String contextPath;

  @Getter
  @Setter
  private String operationId;

  @Getter
  @Setter
  private String modelsToGenerate;

  @Getter
  @Setter
  private boolean skipGenerationRoutes;

  @Getter
  @Setter
  private boolean skipGenerationModels;

  public ServiceRegistryProperties() {
    // Default Constructor
  }

  public String getUri() {
    if (this.serviceName != null && !this.serviceName.isEmpty()) {
      return this.serviceName;
    } else {
      return this.ip + ":" + this.port;
    }
  }
}
