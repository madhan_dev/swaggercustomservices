/*******************************************************************************
 * Copyright ©2002-2018 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/

package com.skava.plugins;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import io.swagger.codegen.languages.JavaCXFServerCodegen;

public class CxfCodeGen extends JavaCXFServerCodegen {

  Set<String> reserveModelNames;

  public CxfCodeGen() {
    super();
    this.embeddedTemplateDir = this.templateDir = "ApacheCXFJaxRS";

    reserveModelNames = new HashSet<>(
      Arrays.asList(
        "String",
        "boolean",
        "Boolean",
        "Double",
        "double",
        "Integer",
        "int",
        "Long",
        "long",
        "Float",
        "float",
        "Object",
        "BigDecimal",
        "Map",
        "file"));
  }

  @Override
  public String getHelp() {
    return "Generates a Java JAXRS Server application based on Apache CXF framework.";
  }

  @Override
  public String getName() {
    return "jaxrs";
  }

  @Override
  public void processOpts() {
    super.processOpts();
    sourceFolder = System.getProperty("sourceFolder", "gen" + File.separator + "java");

    modelTemplateFiles.clear();
    modelTemplateFiles.put("dto.mustache", ".java");

    apiTemplateFiles.clear();

    supportingFiles.clear();

    apiTestTemplateFiles.clear();

    /**
     * whyblocktypemapingaddeddfortypeMapping.put("Long", "long");
     * typeMapping.put("Boolean", "boolean");
     * typeMapping.put("Double", "double");
     * typeMapping.put("Integer", "int");
     * typeMapping.put("Float", "float");
     */
    /*
     * if (inputSpec.indexOf("listservices") >= 0) {
     * typeMapping.put("List", "java.util.List");
     * importMapping.remove("List");
     * }
     */
  }

  @Override
  public String toModelName(String name) {
    // model name cannot use reserved keyword, e.g. return
    if (reservedWords.contains(name)) {
      throw new RuntimeException(name + " (reserved word) cannot be used as a model name");
    }
    if (reserveModelNames.contains(name)) {
      return camelize(name);
    }
    // camelize the model name
    // phone_number => PhoneNumber
    return camelize(name);
  }

}
