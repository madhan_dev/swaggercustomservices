/*******************************************************************************
 * Copyright ©2002-2018 Skava.
 * All rights reserved.The Skava system, including
 * without limitation, all software and other elements
 * thereof, are owned or controlled exclusively by
 * Skava and protected by copyright, patent, and
 * other laws. Use without permission is prohibited.
 * 
 * For further information contact Skava at info@skava.com.
 ******************************************************************************/
package com.skava.plugins;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.codehaus.plexus.util.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class RouteGenerator.
 */
public class RouteGenerator {

  /** The Constant OPERATION_ID. */
  private static final String OPERATION_ID = "operationId";

  /** Logger. */
  private static final Logger LOGGER = LoggerFactory.getLogger(RouteGenerator.class);

  /** The Constant SUCCESSCODE_START. */
  private static final int SUCCESSCODE_START = 200;

  /** The Constant SUCCESSCODE_END. */
  private static final int SUCCESSCODE_END = 299;

  /** The Constant packagePrefix. */
  private static final String PACKAGEPREFIX = "com.skava";

  /** The package name. */
  private static String packageName;

  /** The allowed operation id. */
  private static String allowedOperationId;

  protected RouteGenerator() {

  }

  /**
  * Read all.
  *
  * @param rd the rd
  * @return the string
  * @throws IOException Signals that an I/O exception has occurred.
  */
  private static String readAll(Reader rd) throws IOException {
    StringBuilder sb = new StringBuilder();
    int cp;
    while ((cp = rd.read()) != -1) {
      sb.append((char) cp);
    }
    return sb.toString();
  }

  /**
   * Read json from url.
   *
   * @param url the url
   * @return the JSON object
   * @throws IOException Signals that an I/O exception has occurred.
   */
  private static JSONObject readJsonFromUrl(String url) throws IOException {
    try (InputStream is = new URL(url).openStream()) {
      return new JSONObject(readAll(new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")))));
    }
  }

  /**
   * The main method.
   *
   * @param args the arguments
   */
  public static void generateRoutes(String sourceFile, String packageSource, String modelSuffix, String xmlPackage) {

    JSONObject json = null;
    Map<String, ServiceRegistryProperties> entries = new HashMap<>();
    try {
      entries = loadProperties(sourceFile);
    } catch (IOException e1) {
      LOGGER.error("Exception on load propertis {}", e1);
    }
    FileOutputStream ostream = null;
    Iterator<Map.Entry<String, ServiceRegistryProperties>> iterator = entries.entrySet().iterator();
    while (iterator.hasNext()) {
      Map.Entry<String, ServiceRegistryProperties> entry = iterator.next();
      // TODO: System.out.println("entry key = " + entry.getKey() + "entry value=" + entry.getValue().getContextPath());
      if (!entry.getValue().isSkipGenerationRoutes()) {
        try {
          json = readJsonFromUrl(
            entry.getValue().getIp() + "/" + entry.getValue().getContextPath() + "/v2/api-docs");
        } catch (IOException ee) {
          LOGGER.info("URL exception", ee);
        }
        String serviceNameInStore = entry.getKey();
        allowedOperationId = entry.getValue().getOperationId();
        packageName = (packageSource != null ? packageSource : PACKAGEPREFIX) + serviceNameInStore + "." + modelSuffix;

        if (json != null) {
          JSONObject paths = json.getJSONObject("paths");
          Iterator<?> keys = paths.keys();

          Map<String, Object> toSort = new TreeMap<>();

          while (keys.hasNext()) {
            String mapping = (String) keys.next();
            toSort.put(mapping, paths.get(mapping));
          }

          StringBuilder routes = new StringBuilder("<routes xmlns=\"http://camel.apache.org/schema/spring\">\n");
          constructRoutes(routes, serviceNameInStore, toSort);
          routes.append("</routes>");

          ostream = createFile(xmlPackage + serviceNameInStore + File.separator + serviceNameInStore + "-routes.xml",
            routes);
        }
      }
    }
    if (ostream != null) {
      try {
        ostream.close();
      } catch (IOException e) {
        LOGGER.error("Exception on ostream");
      }
    }
  }

  /**
   * @param sprops
   * @param splitArr
   */
  private static void setServiceProps(ServiceRegistryProperties sprops, String[] splitArr, String value) {
    switch (splitArr[1]) {
      case "ip":
        sprops.setIp(value);
        break;
      case "contextPath":
        sprops.setContextPath(value);
        break;
      case "port":
        sprops.setPort(value);
        break;
      case "serviceName":
        sprops.setServiceName(value);
        break;
      case "version":
        sprops.setVersion(value);
        break;
      case OPERATION_ID:
        sprops.setOperationId(value);
        break;
      case "modelsToGenerate":
        sprops.setModelsToGenerate(value);
        break;
      case "skipGenerationRoutes":
        sprops.setSkipGenerationRoutes(Boolean.parseBoolean(value));
        break;
      case "skipGenerationModels":
        sprops.setSkipGenerationModels(Boolean.parseBoolean(value));
        break;
      default:
        break;
    }
  }

  /**
  * Construct routes.
  *
  * @param routes the routes
  * @param serviceName the service name
  * @param serviceNameInStore the service name in store
  * @param context the context
  * @param toSort the to sort
  */
  private static void constructRoutes(StringBuilder routes, String serviceNameInStore, Map<String, Object> toSort) {
    for (Map.Entry<String, Object> entry : toSort.entrySet()) {
      String mapping = entry.getKey();
      Object map = entry.getValue();
      contructControllerRoutes(routes, serviceNameInStore, mapping, map);
    }
  }

  /**
   * Contruct controller routes.
   *
   * @param routes the routes
   * @param serviceName the service name
   * @param serviceNameInStore the service name in store
   * @param context the context
   * @param mapping the mapping
   * @param map the map
   */
  private static void contructControllerRoutes(StringBuilder routes, String serviceNameInStore, String mapping,
    Object map) {
    if (map instanceof JSONObject) {
      Iterator<?> methods = ((JSONObject) map).keys();
      constructEndpoint(routes, serviceNameInStore, mapping, (JSONObject) map, methods);
    }
  }

  /**
   * Construct endpoint.
   *
   * @param routes the routes
   * @param serviceName the service name
   * @param serviceNameInStore the service name in store
   * @param context the context
   * @param mapping the mapping
   * @param map the map
   * @param methods the methods
   */
  private static void constructEndpoint(StringBuilder routes, String serviceNameInStore, String mapping, JSONObject map,
    Iterator<?> methods) {
    while (methods.hasNext()) {
      String mappingNames = (String) methods.next();
      Object method = map.get(mappingNames);
      if (method instanceof JSONObject) {
        String opId = ((JSONObject) method).get(OPERATION_ID).toString();
        if (allowedOperationId == null || (allowedOperationId != null && allowedOperationId.contains(opId))) {
          opId = StringUtils.deleteWhitespace(opId);
          StringBuilder route = new StringBuilder(
            "\t<route id=\"" + serviceNameInStore
              + WordUtils.capitalize(opId)
              + "Route\">\n");
          route
            .append("\t\t<from uri=\"direct:" + serviceNameInStore
              + WordUtils.capitalize(opId)
              + "\"/>\n");
          route.append(
            "\t\t<log message=\"Route Started - $simple{routeId} - $simple{threadName}\" loggingLevel=\"DEBUG\"/>\n");
          route.append("\t\t<setHeader headerName=\"Exchange.HTTP_PATH\">\n\t\t\t<simple/>\n\t\t</setHeader>\n");
          if ("GET".equalsIgnoreCase(mappingNames)) {
            route.append("\t\t<setBody><simple/></setBody>\n");
          } else if ("POST".equalsIgnoreCase(mappingNames) || "PUT".equalsIgnoreCase(mappingNames) ||
            "PATCH".equalsIgnoreCase(mappingNames)) {
            route.append("\t\t<marshal ref=\"serializer\"/>\n");
          }
          route.append("\t\t<setHeader headerName=\"x-collection-id\">\n\t\t\t<simple>${exchangeProperty." +
            serviceNameInStore + "CollectionId}</simple>\n\t\t</setHeader>\n");
          route.append(
            "\t\t<setHeader headerName=\"x-version-id\">\n\t\t\t<simple>${in.headers"
              + ".x-version}</simple>\n\t\t</setHeader>\n");
          route.append("\t\t<setHeader headerName=\"Exchange.HTTP_METHOD\">\n\t\t\t<constant>"
            + mappingNames.toUpperCase(
              Locale.ENGLISH)
            + "</constant>\n\t\t</setHeader>\n");

          constructParameters(serviceNameInStore, mapping, (JSONObject) method, route);
          constructResponseObjects(serviceNameInStore, (JSONObject) method, route);
          route.append("\n\t</route>\n\n");
          routes.append(route);
        }
      }
    }
  }

  /**
   * Construct response objects.
   *
   * @param method the method
   * @param route the route
   */
  private static void constructResponseObjects(String serviceNameInStore, JSONObject method, StringBuilder route) {
    if (method.has("responses")) {
      JSONObject responses = (JSONObject) method.get("responses");
      Set<?> keys = responses.keySet();
      for (Object key : keys) {
        String responseCode = (String) key;
        int code = Integer.parseInt(responseCode);
        if (code >= SUCCESSCODE_START && code <= SUCCESSCODE_END
          && responses.getJSONObject(responseCode).has("schema")) {
          JSONObject schema = responses.getJSONObject(responseCode).getJSONObject("schema");
          String useList = "";
          if (schema.has("items") && schema.has("type") && schema.getString("type").equalsIgnoreCase("array")) {
            schema = schema.getJSONObject("items");
            useList = " useList=\"true\"";
          }
          appendResponse(serviceNameInStore, route, schema, code, useList);
        }
      }
    }

  }

  /**
   * Append response.
   *
   * @param method the method
   * @param route the route
   * @param schema the schema
   */
  private static void appendResponse(String serviceNameInStore, StringBuilder route, JSONObject schema,
    int code, String useList) {
    if (schema.has("$ref")) {
      String ref = schema.getString("$ref");
      ref = ref.replace("#/definitions/", "");
      route.append("\n\t\t<choice>\n\t\t\t<when>\n\t\t\t\t<simple>${in.headers.CamelHttpResponseCode} == " + code +
        "</simple>\n\t\t\t\t<log message=\"Success - $simple{routeId} - $simple{threadName}\" loggingLevel=\"DEBUG\"/>"
        + "\n\t\t\t\t<unmarshal>\n\t\t\t\t\t<json library=\"Jackson\" unmarshalTypeName=\""
        + packageName
        + "." + ref + "\"" + useList + "/>\n\t\t\t\t</unmarshal>\n\t\t\t</when>"
        + "\n\t\t\t<otherwise>" + "\n\t\t\t\t<choice>\n\t\t\t\t\t<when>\n\t\t\t\t\t\t<simple>${exchangeProperty."
        + serviceNameInStore + "OverrideErrorResponse} != true</simple>"
        + "\n\t\t\t\t\t\t<log message=\"Failure - $simple{routeId} - $simple{threadName}\" loggingLevel=\"DEBUG\"/>"
        + "\n\t\t\t\t\t\t<process ref=\"commonErrorHandler\" />\n\t\t\t\t\t\t"
        + "<stop/>\n\t\t\t\t\t</when>\n\t\t\t\t\t<otherwise>\n\t\t\t\t\t\t"
        + "<log message=\"Failure - $simple{routeId} - $simple{threadName}\" loggingLevel=\"DEBUG\"/>"
        + "\n\t\t\t\t\t\t<setProperty propertyName=\""
        + serviceNameInStore + "OverrideErrorResponse\">"
        + "\n\t\t\t\t\t\t\t<simple>false</simple>\n\t\t\t\t\t\t</setProperty>"
        + "\n\t\t\t\t\t</otherwise>\n\t\t\t\t</choice>\n\t\t\t</otherwise>\n\t\t</choice>");
    }
  }

  /**
   * Construct query parameter.
   *
   * @param mapping the mapping
   * @param params the params
   * @param route the route
   */
  private static void constructQueryParameter(JSONArray params, StringBuilder route) {

    int j = 0;
    StringBuilder query = new StringBuilder();
    for (int i = 0; i < params.length(); i++) {
      JSONObject object = (JSONObject) params.get(i);
      if ("query".equalsIgnoreCase((String) object.get("in"))) {
        if (j == 0) {
          j += 1;
          query.append((object.get("name")) + "=${in.headers." + (object.get("name")) + "}");
        } else {
          query.append("&amp;" + (object.get("name")) + "=${in.headers." + (object.get("name")) + "}");
        }
      }
    }
    if (j > 0) {
      route.append(
        "\t\t"
          + "<setHeader headerName=\"Exchange.HTTP_QUERY\">\n\t\t\t<simple>"
          + query.toString()
          + "</simple>\n\t\t</setHeader>\n");
    } else {
      route.append(
        "\t\t<setHeader headerName=\"Exchange.HTTP_QUERY\">\n\t\t\t<simple/>\n\t\t</setHeader>\n");
    }
  }

  /**
   * Construct parameters.
   *
   * @param serviceName the service name
   * @param context the context
   * @param mapping the mapping
   * @param method the method
   * @param route the route
   */
  private static void constructParameters(String serviceName, String mapping, JSONObject method,
    StringBuilder route) {
    if (method.has("parameters")) {
      JSONArray params = (JSONArray) method.get("parameters");
      constructQueryParameter(params, route);
    }
    route.append(
      "\t\t<setHeader headerName=\"Exchange"
        + ".HTTP_URI\">\n\t\t\t<simple>${bean:orchestrationConfiguration?method=getURL"
        + "(" + serviceName + ")}/" + "{{skava.services." + serviceName + ".contextPath}}"
        + mapping.replace("{", "${in.headers.")
        + "</simple>\n\t\t</setHeader>\n\t\t<to uri=\"{{skava.component.http}}\"/>");
  }

  /**
  * @return
  * @throws IOException
  * @throws FileNotFoundException
  */
  public static Map<String, ServiceRegistryProperties> loadProperties(String defaultConfigPath) throws IOException {
    Properties defaultProps = new Properties();
    defaultProps.load(new FileInputStream(defaultConfigPath));
    TreeSet<Object> keySet = new TreeSet<>(defaultProps.keySet());
    ServiceRegistryProperties sprops = null;
    Map<String, ServiceRegistryProperties> entries = new HashMap<>();
    for (Object obj : keySet) {
      if (obj.toString().startsWith("skava.services.")) {
        String splitStr = obj.toString().replace("skava.services.", "");
        String[] splitArr = splitStr.split("\\.");
        // TODO: System.out.println("key1=" + splitArr[0] + "/key2=" + splitArr[1] + "/object=" + obj.toString());
        if (!entries.isEmpty() && entries.containsKey(splitArr[0]) && sprops != null) {
          setServiceProps(sprops, splitArr, defaultProps.getProperty(obj.toString()).trim());
          entries.put(splitArr[0].trim(), sprops);
          // TODO: System.out.println("key=" + splitArr[0] + "/value=" + sprops.getContextPath());
        } else {
          sprops = new ServiceRegistryProperties();
          setServiceProps(sprops, splitArr, defaultProps.getProperty(obj.toString()).trim());
          entries.put(splitArr[0].trim(), sprops);
          // TODO: System.out.println("key=" + splitArr[0] + "/value=" + sprops.getContextPath());
        }
      }
    }
    return entries;
  }

  /**
   * Create file if not exist.
   *
   * @param path For example: "D:\foo.xml"
   */
  public static FileOutputStream createFile(String path, StringBuilder routes) {
    FileOutputStream ostream = null;
    try {
      File file = new File(path);
      if (!file.exists()) {
        file.getParentFile().mkdirs();
        if (file.createNewFile()) {
          ostream = writeToFile(path, routes);
        } else {
          ostream = writeToFile(path, routes);
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return ostream;
  }

  /**
   * @param path
   * @param routes
   * @throws FileNotFoundException
   * @throws IOException
   */
  private static FileOutputStream writeToFile(String path, StringBuilder routes) throws IOException {
    FileOutputStream writer = new FileOutputStream(path, false);
    writer.write(routes.toString().replaceAll("\t", "    ").getBytes());
    return writer;
  }
}
